#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR/..
sed "s/'2368'/process.env.PORT || '2368'/g;s/127.0.0.1/0.0.0.0/g" config.example.js > config.temp.js
mv config.temp.js config.js
